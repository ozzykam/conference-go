import json
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Conference, Location, State
from django.views.decorators.http import require_http_methods
from .acls import get_photo, get_weather

class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties =[
        "id",
        "name",
    ]

@require_http_methods(["POST", "GET"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location ID"},
                status=400,
            )
        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceListEncoder,
            safe=False,
        )




class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
    ]
    # encoders = {
    #     "location": LocationListEncoder(),
    # }
    def get_extra_data(self, o):
        return {
            "location": {
                "event_center_name": o.location.name,
                "city": o.location.city, 
                "state": o.location.state.abbreviation,
            }
        }


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_conference(request, id):
    if request.method == "GET":
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location ID"},
                status=400,
            )
        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)
        weather = get_weather(conference.location.city, conference.location.state.abbreviation)
        content.update(weather)
        return JsonResponse(
            {
                "weather": weather,
                "conference": conference,
            },
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


class ListLocationEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        
    ]

@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=ListLocationEncoder,
    )
    else:
        content = json.loads(request.body)
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        photo = get_photo(content["city"], content["state"])
        content.update(photo)

        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
    )



class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",
    ]
    
    def get_extra_data(self, o):
        return { "state": o.state.abbreviation }

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_location(request, id):
    """
    =====================================================
    location_details = [
        {
            "name": l.name,
            "city": l.city,
            "room_count": l.room_count,
            "created": l.created,
            "updated": l.updated,
            "state": l.state.abbreviation,
        }
        for l in Location.objects.filter(id=id)
    ]

    return JsonResponse({"location_details": location_details})
    =====================================================
    """
    if request.method == "GET":
        location = Location.objects.get(id=id)
        weather = get_weather(location.city, location.state)
        return JsonResponse(
            {
                "weather": weather,
                "info": location,
            },
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count >0 })
    
    else:
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        photo = get_photo(content["city"], content["state"])
        content.update(photo)
        

        Location.objects.filter(id=id).update(**content)
        location = Location.objects.get(id=id)
        
        weather = get_weather(location.city, location.state)
        content.update(weather)
        return JsonResponse(
            {
                "weather": weather,
                "info": location,
            },
            encoder=LocationDetailEncoder,
            safe=False,
        )





# @require_http_methods(["GET", "DELETE", "PUT"])
# def api_show_conference(request, id):
#     if request.method == "GET":
#         conference = Conference.objects.get(id=id)  #------------------- Create the new Entity instance (for GET)
#         return JsonResponse(   #---------------------------------------- Return the new Entity in a JsonResponse (for GET)
#             conference,
#             encoder=ConferenceDetailEncoder,
#             safe=False,
#         )
#     elif request.method == "DELETE":
#         count, _ = Conference.objects.filter(id=id).delete()
#         return JsonResponse({"deleted": count > 0}) #------------------- Return the new Entity in a JsonResponse (for DELETE)
#     else:
#         content = json.loads(request.body)  #--------------------------- Decode the JSON into a dictionary
#         try:
#             location = Location.objects.get(id=content["location"]) #--- Translate any properties into model objects
#             content["location"] = location  #--------------------------- Create the new Entity instance (attribute set for PUT)
#         except Location.DoesNotExist:#---------------------------------- Handle any errors that could happen
#             return JsonResponse(
#                 {"message": "Invalid location ID"},
#                 status=400,
#             )
#         Conference.objects.filter(id=id).update(**content)
#         conference = Conference.objects.get(id=id)#--------------------- Create the new Entity instance (updated for PUT) 
#         return JsonResponse(    #--------------------------------------- Return the new Entity in a JsonResponse (for PUT)
#             conference,
#             encoder=ConferenceDetailEncoder,
#             safe=False,
#         )   #----------------------------------------------------------- Return the new Entity in a JsonResponse (for PUT)