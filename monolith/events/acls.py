import json
from random import randint
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
from django.http import JsonResponse

def get_photo(city, state):
    headers = {"Authorization":  PEXELS_API_KEY}
    params = {
        "page": randint(1,10),
        "per_page": 1,
        "query": f"{city}, {state} skyline"
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    
    content = response.json()

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather(city, state):
    headers = {"Authorization": OPEN_WEATHER_API_KEY}
    params = {
        "q": f"{city}, {state},US",
        "appid": OPEN_WEATHER_API_KEY
    }
    url = "http://api.openweathermap.org/geo/1.0/direct?"
    response = requests.get(url, params=params, headers=headers)

    content = response.json()


    lat = content[0]["lat"] 
    lon= content[0]["lon"]
    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial"
    }
    url = "https://api.openweathermap.org/data/2.5/weather?"
    response = requests.get(url,params=params, headers=headers)

    content = response.json()

    try:
        return {
            "temp": content["main"]["temp"],
            "description": content["weather"][0]["description"],
        }
    except (KeyError, IndexError):
        return {"weather": None}